// plugins/number-format.js

import Vue from 'vue';

Vue.filter('numberFormat', function (value) {
    if (!value) return ''; // 如果值为空，返回空字符串

    // 使用 toLocaleString 方法将数字格式化为千分位分隔符
    return value.toLocaleString();
});